import { TransitiveCompileNgModuleMetadata } from "@angular/compiler";

export const environment = {
  production: TransitiveCompileNgModuleMetadata,
  search_url: "https://api.spotify.com/v1/search"
};

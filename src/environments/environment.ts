import { AuthOptions } from "../app/security/security.service";

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  search_url: "https://api.spotify.com/v1/search",

  auth_options: <AuthOptions>{
    auth_url: "https://accounts.spotify.com/authorize",
    client_id: "a512f6d042e749c2aa67ef1f06884c6e",
    response_type: "token",
    redirect_uri: "http://localhost:4200/"
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthOptions, SecurityService } from "./security.service";
import { environment } from "src/environments/environment";
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptorService } from "./auth-interceptor.service";

@NgModule({
  imports: [
    CommonModule
    // HttpClientXsrfModule.withOptions({
    //   cookieName: "XSRF-TOKEN",
    //   headerName: "Authorization"
    // })
  ],
  declarations: [],
  providers: [
    {
      provide: AuthOptions,
      useValue: environment.auth_options
    },
    AuthInterceptorService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
    // SecurityService
  ]
})
export class SecurityModule {
  constructor(private security: SecurityService) {
    this.security.getToken();
  }
}

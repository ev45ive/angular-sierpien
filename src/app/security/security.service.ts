import { Injectable, Inject, InjectionToken } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthOptions {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class SecurityService {
  constructor(private authOptions: AuthOptions) {}

  authorize() {
    const { client_id, response_type, redirect_uri } = this.authOptions;

    const params = new HttpParams({
      fromObject: {
        client_id,
        response_type,
        redirect_uri
      }
    }).toString();

    localStorage.removeItem("token");
    const url = this.authOptions.auth_url + "?" + params;
    window.location.replace(url);
  }

  token = "";

  getToken() {
    this.token = JSON.parse(localStorage.getItem("token"));

    if (!this.token && window.location.hash) {
      const params = new HttpParams({
        fromString: window.location.hash.substr(1)
      });
      this.token = params.get("access_token");
      localStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}

import { Component, OnInit, HostBinding, Input, Output } from '@angular/core';
import { Album } from 'src/app/model/album.interface';

@Component({
  selector: 'app-album-item',
  templateUrl: './album-item.component.html',
  styleUrls: ['./album-item.component.css']
})
export class AlbumItemComponent implements OnInit {

  @Input()
  album:Album

  @HostBinding('class.card')
  cardClass = true


  constructor() { }

  ngOnInit() {
  }

}

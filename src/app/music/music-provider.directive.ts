import { Directive, SkipSelf, Optional, Self } from "@angular/core";
import { MusicService } from "./music.service";
import { combineLatest } from "rxjs";

@Directive({
  selector: "[appMusicProvider]",
  providers: [MusicService]
})
export class MusicProviderDirective {
  constructor(
    @SkipSelf() @Optional() private parent: MusicService,
    @Self() private local: MusicService
  ) {
    combineLatest(local.query$, parent.query$, console.log).subscribe();
  }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicRoutingModule } from "./music-routing.module";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumItemComponent } from "./album-item/album-item.component";
import { environment } from "../../environments/environment";
import { MusicService, MUSIC_SEARCH_URL } from "./music.service";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MusicProviderComponent } from './music-provider/music-provider.component';
import { AlbumDetailsComponent } from './album-details/album-details.component';
import { AlbumContainerComponent } from './album-container/album-container.component';
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [
    CommonModule, 
    MusicRoutingModule, 
    HttpClientModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumItemComponent,
    MusicProviderComponent,
    AlbumDetailsComponent,
    AlbumContainerComponent
  ],
  exports: [MusicSearchComponent, MusicProviderComponent],
  providers: [
    {
      provide: MUSIC_SEARCH_URL,
      useValue: environment.search_url
    }
    // {
    //   provide:'MusicService',
    //   useFactory:(music_url)=>{
    //     return new MusicService(music_url)
    //   },
    //   deps:['MUSIC_SEARCH_URL']
    // },
    // {
    //   provide: "MusicService",
    //   useClass: MusicService,
    //   // deps: ["MUSIC_SEARCH_URL"]
    // },
    // {
    //   provide: AbstractMusicService,
    //   useClass: SpecialChistmassMusicService,
    // },
    // MusicService,
    // {
    //   provide:'X',
    //   useExisting:MusicService
    // }
  ]
})
export class MusicModule {}

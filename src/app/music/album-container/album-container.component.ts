import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MusicService } from "../music.service";
import { map } from "rxjs/operators";

@Component({
  selector: "app-album-container",
  templateUrl: "./album-container.component.html",
  styleUrls: ["./album-container.component.css"]
})
export class AlbumContainerComponent implements OnInit {
  constructor(
    private musicService: MusicService,
    private route: ActivatedRoute
  ) {}

  album;

  loadAlbum(id: string) {
    this.musicService.loadAlbum(id).subscribe(album => {
      this.album = album;
    });
  }

  ngOnInit() {
    // this.route.paramMap
    //   .pipe(map(params => params.get("id")))
    //   .subscribe(id => this.loadAlbum(id));
    this.route.data.subscribe(data => (this.album = data["album"]));
  }
}

import {
  async,
  ComponentFixture,
  TestBed,
  inject,
  fakeAsync,
  tick
} from "@angular/core/testing";
import {
  RouterTestingModule,
  setupTestingRouter
} from "@angular/router/testing";
import { AlbumContainerComponent } from "./album-container.component";
import { Routes, Router } from "@angular/router";
import { Location } from "@angular/common";
import { Component, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { By } from "@angular/platform-browser";
import { MusicService } from "src/app/music/music.service";
import { of } from "rxjs";

@Component({
  template: `<router-outlet></router-outlet>`
})
export class RouterContextComponent {}

@Component({
  selector: "app-album-details",
  template: ``,
  inputs: ["album:album"]
})
export class MockALbumDetailsComponent {}

fdescribe("AlbumContainerComponent", () => {
  let component: AlbumContainerComponent;
  let fixture: ComponentFixture<RouterContextComponent>;

  const routes: Routes = [
    {
      path: "album/:id",
      component: AlbumContainerComponent
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes, {})],
      declarations: [AlbumContainerComponent, RouterContextComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: MusicService,
          useValue: { loadAlbum() {} }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(fakeAsync(
    inject(
      [Router, MusicService],
      (router: Router, musicService: MusicService) => {
        const fixture = TestBed.createComponent(RouterContextComponent);
        router.navigate(["/album", 123]);
        tick();
        component = fixture.debugElement.query(
          By.directive(AlbumContainerComponent)
        ).componentInstance;

        spyOn(musicService, "loadAlbum");
        spyOn(component, "loadAlbum");

        fixture.detectChanges();
      }
    )
  ));

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should load album with id form param", fakeAsync(
    inject([Router, Location], (router: Router, location) => {
      expect(component.loadAlbum).toHaveBeenCalledWith("123");
    })
  ));

  it("should get album from service", inject(
    [MusicService],
    (musicService: MusicService) => {
      (component.loadAlbum as jasmine.Spy).and.callThrough();
      (musicService.loadAlbum as jasmine.Spy).and.returnValue(of({}));

      component.loadAlbum("123");
      expect(musicService.loadAlbum).toHaveBeenCalledWith("123");
    }
  ));
});

import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Album } from "../model/album.interface";
import { Injectable } from "@angular/core";
import { MusicService } from "src/app/music/music.service";

@Injectable()
export class AlbumResolver implements Resolve<Album> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const id = route.paramMap.get("id");

    return this.musicService.loadAlbum(id);
  }

  constructor(private musicService: MusicService) {}
}

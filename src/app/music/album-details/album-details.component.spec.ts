import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { AlbumDetailsComponent } from "./album-details.component";
import { Album } from "../../model/album.interface";
import { By } from "@angular/platform-browser";
import { FormsModule, NgModel } from "@angular/forms";

fdescribe("AlbumDetailsComponent", () => {
  let component: AlbumDetailsComponent;
  let fixture: ComponentFixture<AlbumDetailsComponent>;

  let albumMock: Album;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlbumDetailsComponent],
      imports: [FormsModule],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    albumMock = {
      id: "123",
      name: "Album Name",
      artists: [],
      images: []
    };
    fixture = TestBed.createComponent(AlbumDetailsComponent);
    component = fixture.componentInstance;
    component.album = albumMock;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("should display album name", () => {
    component.album = albumMock;

    const elem = fixture.debugElement.query(By.css(".card-title"));
    expect(elem.nativeElement.innerText).toMatch(albumMock.name);
  });

  it("should display updated album name", () => {
    component.album.name = "Updated";
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css(".card-title"));
    expect(elem.nativeElement.innerText).toMatch("Updated");
  });

  it("should display album name form", () => {
    const input = fixture.debugElement.query(By.css("input"));
    component.album.name = "Updated";
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(input.nativeElement.value).toEqual("Updated");
    });
  });

  it("should update album name when name input changes", () => {
    const input = fixture.debugElement.query(By.directive(NgModel));
    input.nativeElement.value = "Updated";

    input.triggerEventHandler("input", {
      target: input.nativeElement
    });
    /* OR : */
    input.nativeElement.dispatchEvent(new Event("input"));

    fixture.whenStable().then(() => {
      expect(component.album.name).toEqual("Updated");
    });
  });

  it("should save when save button is pressed", () => {
    const button = fixture.debugElement.query(By.css(".save-button"));

    const spy = spyOn(component, "save")

    button.triggerEventHandler("click", {});

    expect(spy).toHaveBeenCalled();
  });

  it("should emit event when album is saved", () => {
    const spy = jasmine.createSpy('albumChange')
    
    component.albumChange.subscribe(spy)
    component.save()

    expect(spy).toHaveBeenCalledWith(component.album)
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicProviderComponent } from './music-provider.component';

describe('MusicProviderComponent', () => {
  let component: MusicProviderComponent;
  let fixture: ComponentFixture<MusicProviderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MusicProviderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MusicProviderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

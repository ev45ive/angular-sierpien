import { Component, OnInit, SkipSelf } from "@angular/core";
import { MusicService } from "src/app/music/music.service";

@Component({
  selector: "app-music-provider",
  templateUrl: "./music-provider.component.html",
  styleUrls: ["./music-provider.component.css"],
  providers: [MusicService]
})
export class MusicProviderComponent implements OnInit {
  constructor(@SkipSelf() private parent: MusicService, private local: MusicService) {
    console.log(parent,local)
  }

  ngOnInit() {}
}

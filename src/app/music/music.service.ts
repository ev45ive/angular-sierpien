import {
  Injectable,
  Inject,
  InjectionToken,
  EventEmitter
} from "@angular/core";
import { Album, AlbumsResponse } from "src/app/model/album.interface";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { SecurityService } from "../security/security.service";

import {
  map,
  catchError,
  startWith,
  switchMap,
  switchAll,
  publish,
  refCount
} from "rxjs/operators";
import { throwError, Observable, of, Subject, BehaviorSubject } from "rxjs";
import { distinctUntilChanged } from "rxjs/operators";

export const MUSIC_SEARCH_URL = new InjectionToken<string>(
  "Url for music search service"
);

@Injectable({
  providedIn: "root"
})
export class MusicService {
  albums$ = new BehaviorSubject<Album[]>([]);
  query$ = new BehaviorSubject<string>("batman");
  // query = this.query$.asObservable()

  queryAlbums;

  loadAlbum(id:string){
    return this.http.get<Album>(
      `https://api.spotify.com/v1/albums/${id}`
    )
  }

  constructor(
    private http: HttpClient,
    @Inject(MUSIC_SEARCH_URL) private search_url: string
  ) {
    console.log(this.albums$)
    this.queryAlbums = this.query$
      .pipe(
        distinctUntilChanged(),
        map(query => ({
          q: query,
          type: "album",
          limit: "20"
        })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.search_url, {
            params,
            // withCredentials:true
          })
        ),
        map(response => response.albums.items)
        // publish(),
        // refCount()
      )
      .subscribe(albums => {
        this.albums$.next(albums);
      });
  }

  search(query = "batman"): void {
    this.query$.next(query);
  }

  getAlbums(): Observable<Album[]> {
    // this.albums$.getValue()
    return this.albums$.asObservable();
    // return this.queryAlbums;
  }

  getQuery() {
    return this.query$.asObservable();
  }
}

import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormArray,
  FormBuilder,
  Validators,
  Validator,
  ValidatorFn,
  ValidationErrors,
  AbstractControl,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  combineLatest,
  mapTo,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer } from "rxjs";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.css"]
})
export class SearchFormComponent implements OnInit {
  searchForm: FormGroup;

  @Input()
  set query(query) {
    this.searchForm.get("query").setValue(query, {
      // emitEvent: false
    });
  }

  constructor(private fb: FormBuilder) {
    const censor = (badword: string): ValidatorFn => (
      c: AbstractControl
    ): ValidationErrors | null => {
      const hasError = (c.value as string).includes(badword);

      return hasError ? { censor: badword } : null;
    };

    const asyncCensor = (badword: string): AsyncValidatorFn => (
      c: AbstractControl
    ) => {
      // return this.http.get('/validate?q='+c.value).pipe(map(x=>x))

      return Observable.create(
        (observer: Observer<ValidationErrors | null>) => {
          /* Subscribe logic: */
          const handler = setTimeout(() => {
            const hasError = (c.value as string).includes(badword);
            observer.next(hasError ? { censor: badword } : null);
            observer.complete();
          }, 1000);

          /* Unsubscribe logic: */
          return () => {
            clearTimeout(handler);
          };
        }
      );
    };

    this.searchForm = fb.group({
      query: fb.control(
        "",
        [Validators.required, Validators.minLength(3) /* , censor("batman") */],
        [asyncCensor("batman")]
      )
    });

    // console.log(this.searchForm);

    const queryField = this.searchForm.get("query");

    const status$ = queryField.statusChanges;

    const value$ = queryField.valueChanges;
    // .pipe(debounceTime(400),  distinctUntilChanged());

    const valid$ = status$.pipe(
      filter(status => status === "VALID"),
      mapTo(true)
    );

    valid$
      // .pipe(combineLatest(value$, (valid, value) => value))
      .pipe(withLatestFrom(value$, (valid, value) => value))
      .subscribe(query => {
        this.search(query);
      });
  }

  @Output()
  queryChange = new EventEmitter<string>();

  search(query) {
    this.queryChange.emit(query);
  }

  ngOnInit() {}
}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { AlbumContainerComponent } from "./album-container/album-container.component";
import { AlbumGuard } from "./album-guard";
import { AlbumResolver } from "./album-resolver";

const routes: Routes = [
  {
    path: "music",
    component: MusicSearchComponent
  },
  {
    path: "music/:id",
    component: AlbumContainerComponent,
    canActivate: [AlbumGuard],
    resolve:{
      album: AlbumResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  providers: [AlbumGuard,AlbumResolver],
  exports: [RouterModule]
})
export class MusicRoutingModule {}

import { Component, OnInit, Inject } from "@angular/core";
import { Album } from "src/app/model/album.interface";
import { MusicService } from "../music.service";
import { Subscription, Subject } from "rxjs";
import { takeUntil, tap } from "rxjs/operators";
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-music-search",
  templateUrl: "./music-search.component.html",
  styleUrls: ["./music-search.component.css"]
  // viewProviders:[
  //   MusicService
  // ]
})
export class MusicSearchComponent implements OnInit {
  query$ = this.musicService.getQuery();
  albums$ = this.musicService
    .getAlbums()
    .pipe(tap(albums => (this.albumCount = albums.length)));

  albumCount;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private musicService: MusicService
  ) {}

  search(query) {
    this.router.navigate([], {
      queryParams: { query },

      relativeTo: this.route
    });

    this.musicService.search(query);
  }

  ngOnInit() {
    const query = this.route.snapshot.queryParamMap.get("query");
    if (query) {
      this.search(query);
    }
  }
}

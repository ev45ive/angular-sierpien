export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * Hex color 
   */
  color: string;
  // tracks: Array<Track>;
  // lub:
  tracks?: Track[];
}

interface Track {
  id: number;
}

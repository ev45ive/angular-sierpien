import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "**",
    redirectTo: "playlists",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: true
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';

import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { HighlightDirective } from './shared/highlight.directive';
import { MusicModule } from './music/music.module';
import { SecurityModule } from './security/security.module';
import { MusicProviderDirective } from './music/music-provider.directive';
import { AppRoutingModule } from './/app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    MusicProviderDirective
  ],
  imports: [
    SecurityModule,
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    MusicModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [MusicProviderDirective]
})
export class AppModule { 

  // constructor(private app:ApplicationRef){

  // }
  // ngDoBootstrap(){
  //   this.app.bootstrap(AppComponent)
  // }

}

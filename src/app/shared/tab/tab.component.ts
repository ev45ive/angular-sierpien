import { Component, OnInit, HostBinding } from '@angular/core';
// import { TabsComponent } from '../tabs/tabs.component';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.css']
})
export class TabComponent implements OnInit {

  @HostBinding('class.collapse')
  collapse = true

  @HostBinding('class.show')
  show = true

  // constructor(private tabs:TabsComponent) {
  //   console.log(tabs)
  //  }

  ngOnInit() {
  }

}

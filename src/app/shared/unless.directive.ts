import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  ViewRef
} from "@angular/core";

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  cache: ViewRef;

  @Input("appUnless")
  set appUnless(hide) {
    if (hide) {
      // this.vcr.clear()
      this.cache = this.vcr.detach();
    } else {
      if (this.cache) {
        this.vcr.insert(this.cache);
      } else {
        this.vcr.createEmbeddedView(
          this.tpl,
          // Context: let-zmienna="message"
          {
            $implicit: "Domyslne placki",
            message: "Placki!",
            cache: this.appUnlessCache
          },
          this.vcr.length
        );
      }
    }
  }

  @Input()
  appUnlessCache;

  constructor(private tpl: TemplateRef<any>, private vcr: ViewContainerRef) {}
}

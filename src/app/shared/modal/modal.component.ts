import {
  Component,
  OnInit,
  Input,
  ElementRef,
  HostListener,
  ViewChild,
  Renderer2
} from "@angular/core";

@Component({
  selector: "app-modal",
  templateUrl: "./modal.component.html",
  styleUrls: ["./modal.component.css"]
})
export class ModalComponent implements OnInit {
  @Input()
  title;

  @Input()
  open = false;

  close() {
    this.open = false;
  }

  toggle() {
    this.open = !this.open;
  }

  @ViewChild("dialogModal")
  dialogModal: ElementRef;

  // @HostListener("document:click", ["$event.path"])
  dismiss(path: Element[]) {
    if (
      this.open &&
      this.dialogModal &&
      !path.includes(this.dialogModal.nativeElement)
    ) {
      this.close();
    }
  }

  constructor(private renderer: Renderer2) {
    // renderer.listen(document,'click',()=>{})
    document.addEventListener(
      "click",
      (event:MouseEvent) => {
        this.dismiss(event['path']);
      },
      true
    );
  }

  ngOnInit() {}
}

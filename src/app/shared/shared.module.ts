import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HighlightDirective } from "./highlight.directive";
import { UnlessDirective } from "./unless.directive";
import { TabsComponent } from "./tabs/tabs.component";
import { TabComponent } from "./tab/tab.component";
import { ModalComponent } from "./modal/modal.component";
import { ModalFooterComponent } from "./modal-footer/modal-footer.component";
import { ItemsListComponent } from "../playlists/items-list/items-list.component";

@NgModule({
  imports: [CommonModule],
  declarations: [
    HighlightDirective,
    UnlessDirective,
    TabsComponent,
    TabComponent,
    ModalComponent,
    ModalFooterComponent,
    ItemsListComponent
  ],
  exports: [
    HighlightDirective,
    UnlessDirective,
    TabsComponent,
    TabComponent,
    ModalComponent,
    ModalFooterComponent,
    ItemsListComponent
  ]
})
export class SharedModule {}

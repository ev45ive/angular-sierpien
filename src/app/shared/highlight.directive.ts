import {
  Directive,
  ElementRef,
  Renderer2,
  Input,
  OnInit,
  AfterViewInit,
  OnChanges,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host: {
  //   "[style.color]": "color",
  //   "(mouseenter)":"activate($event)"
  // }
})
export class HighlightDirective implements OnInit, AfterViewInit, OnChanges {
  ngOnInit() {
    // console.log("hello appHighlight", this.color);
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit')
    // this.elem.nativeElement.style.color = this.color
  }

  ngOnChanges(changes) {
    // console.log(changes);
    // this.elem.nativeElement.style.color = this.color
  }

  // set appHighlight(color) {
  // console.log('color change')
  // this.color = color;
  // this.elem.nativeElement.style.color = this.color;
  // }

  @Input("appHighlight")
  appHighlight;

  @HostBinding("style.color")
  @HostBinding("style.border-left-color")
  get currentColor() {
    // console.log('getting color...')
    return this.active ? this.appHighlight : "";
  }

  active = false;

  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x, y) {
    this.active = true;
    // this.color = this.appHighlight;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.active = false;
    // this.color = "";
  }

  constructor(/* private elem: ElementRef, private renderer: Renderer2 */) {
    // console.log('constructor')
    // renderer.setStyle(this.elem.nativeElement,'color','red')
  }
}
// console.log(HighlightDirective);

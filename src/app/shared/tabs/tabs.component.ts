import {
  Component,
  OnInit,
  Input,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.css"]
})
export class TabsComponent implements OnInit {
  @Input()
  set active(index) {
    // this.tabRef.show = !this.tabRef.show;
    if(this.tabs)
    this.tabs.forEach((tab,i)=>{
      if(i == index){
        tab.show = true
      }else{
        tab.show = false
      }
    })
  }

  @ContentChild(TabComponent)
  tabRef: TabComponent;

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  constructor() {
    console.log(this);
  }

  ngOnInit() {}
}

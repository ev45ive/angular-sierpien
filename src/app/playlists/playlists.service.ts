import { Injectable } from "@angular/core";
import { Playlist } from "../model/playlist.interface";
import { BehaviorSubject, combineLatest } from "rxjs";
import { map, tap } from "rxjs/operators";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  playlists = new BehaviorSubject<number[]>([]);
  index = new BehaviorSubject<{
    [id: number]: Playlist;
  }>({});

  constructor(private http: HttpClient) {}

  url = "http://localhost:3000/playlists/";

  fetchPlaylists() {
    this.http.get<Playlist[]>(this.url).subscribe(playlists => {
      const index = this.index.getValue();
      playlists.forEach(playlist => {
        index[playlist.id] = playlist;
      });
      this.index.next(index);
      this.playlists.next(playlists.map(p => p.id));
    });
  }

  getPlaylists() {
    this.fetchPlaylists();
    return combineLatest(this.index, this.playlists, (index, playlists) =>
      playlists.map(id => index[id])
    );
  }

  fetchPlaylist(id: number) {
    this.http.get<Playlist>(this.url + id).subscribe(playlist => {
      this.updatePlaylist(playlist);
    });
  }

  getPlaylist(id: number) {
    // Eager fetch / autofetch
    // this.fetchPlaylist(id);

    return this.index.pipe(map(index => index[id]));
  }

  savePlaylist(playlist: Playlist) {
    this.http
      .put<Playlist>(this.url + playlist.id.toString(), playlist)
      .subscribe(updated => this.updatePlaylist(updated));
  }

  updatePlaylist(playlist: Playlist) {
    const index = this.index.getValue();
    index[playlist.id] = playlist;
    this.index.next(index);
  }
}

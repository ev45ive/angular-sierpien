import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "src/app/model/playlist.interface";

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist;

  mode: "show" | "edit" = "show";

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  save(form) {
    const value = form.value;
    const playlist: Playlist = {
      ...this.playlist,
      ...value
    };
    this.playlistChange.emit(playlist);
  }

  constructor() {}

  ngOnInit() {}
}

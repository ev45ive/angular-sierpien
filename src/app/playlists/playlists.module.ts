import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsComponent } from "./playlists/playlists.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { ItemsListComponent } from "./items-list/items-list.component";
import { ListItemComponent } from "./list-item/list-item.component";
import { FormsModule } from '@angular/forms';
import { SharedModule } from "../shared/shared.module";
import { PlaylistsRoutingModule } from "./playlists-routing.module";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PlaylistsRoutingModule,
    SharedModule
  ],
  // schemas:[
  //   CUSTOM_ELEMENTS_SCHEMA
  // ],
  declarations: [
    PlaylistsComponent,
    PlaylistDetailsComponent,
    ListItemComponent
  ],
  exports: [PlaylistsComponent]
})
export class PlaylistsModule {}

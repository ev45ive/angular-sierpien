import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/model/playlist.interface";
import { Router, ActivatedRoute } from "@angular/router";
import { map, switchMap, filter, tap, share } from "rxjs/operators";
import { PlaylistsService } from "../playlists.service";

@Component({
  selector: "app-playlists",
  templateUrl: "./playlists.component.html",
  styleUrls: ["./playlists.component.css"]
})
export class PlaylistsComponent implements OnInit {
  playlists = this.playlistsService.getPlaylists();

  selected$ = this.route.paramMap.pipe(
    map(paramMap => parseInt(paramMap.get("id"), 10)),
    filter(id => Boolean(id)),
    tap(id => this.playlistsService.fetchPlaylist(id)),
    switchMap(id => this.playlistsService.getPlaylist(id)),
    // Sharing between many async pipes
    share()
  );

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private playlistsService: PlaylistsService
  ) {}

  select(playlist: Playlist) {
    if (playlist) {
      this.router.navigate(["/playlists", playlist.id]);
    } else {
      this.router.navigate(["/playlists"]);
    }
  }

  save(playlist: Playlist) {
    this.playlistsService.savePlaylist(playlist);
  }

  ngOnInit() {}
}

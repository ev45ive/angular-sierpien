import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output,
  ChangeDetectionStrategy
} from "@angular/core";
import { Playlist } from "src/app/model/playlist.interface";

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.css"],
  encapsulation: ViewEncapsulation.Emulated,
  changeDetection: ChangeDetectionStrategy.OnPush
  // inputs:[
  //   'playlists:items'
  // ]
})
export class ItemsListComponent implements OnInit {
  @Input("items")
  playlists: Playlist[];

  @Input()
  selected: Playlist;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    const selected = playlist == this.selected ? null : playlist;
    this.selectedChange.emit(selected);
    // this.selectedChange.subscribe(fn)
  }

  hover;

  constructor() {
    // setInterval(() => {
    //     this.hover = this.playlists[Math.floor(Math.random() * this.playlists.length)];
    //   }, 500);
  }

  trackFn(index, item) {
    return item.id;
  }

  ngOnInit() {}
}
